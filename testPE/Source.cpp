#include "stdafx.h"
#include "catch.hpp"
#include "Source.h"


std::vector<std::string>  readInputFile(std::string fileName) {
	// utf8 kodeering testinmata
	std::string line;
	std::ifstream myfile(fileName);
	std::vector<std::string> result;

	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream iss(line);
			for (std::string s; iss >> s; ) {
				result.push_back(s);
			}
		}
		if (result.size() == 0) {
			throw std::invalid_argument("File is empty");
		}
		return result;
myfile.close();
	}
	else {
		throw std::invalid_argument("Unable to open file");
		return {};
	}
}

std::string& removeChars(std::string& str, const std::string& chars) {
	str.erase(remove_if(str.begin(), str.end(), [&chars](const char& c) {
		return chars.find(c) != std::string::npos;
	}), str.end());
	return str;
}


std::unordered_map<std::string, double> proccessWord(std::string word, int minSnippet)
{
	std::unordered_map<std::string, double> um;
	// remove symbols in the word. "-" possible logic error
	removeChars(word, ",.';:-!?\"");

	int wordSize = word.size();
	if (wordSize >= 3) {
		for (int j = 0; j <= wordSize; ++j) {
			for (int i = 0; i <= wordSize - minSnippet - j; ++i) {

				std::string fragment = word.substr(i, minSnippet + j);
				um.insert({ fragment,  um[fragment] = +um[fragment] + 1 });
			}
		}
	}

	return um;
}

std::unordered_map<std::string, double> mergeMap(std::unordered_map<std::string, double> um1, std::unordered_map<std::string, double> um2)
{
	std::unordered_map<std::string, double> um3 = std::accumulate(um1.begin(), um1.end(), std::unordered_map<std::string, double>(),
		[](std::unordered_map<std::string, double> &m, const std::pair<const std::string, double> &p)
	{
		return (m[p.first] += p.second, m);
	});

	um3 = std::accumulate(um2.begin(), um2.end(), um3,
		[](std::unordered_map<std::string, double> &m, const std::pair<const std::string, double> &p)
	{
		return (m[p.first] += p.second, m);
	});

	return um3;
}

std::vector<std::pair<std::string, double>> sortMaptoVec(std::unordered_map<std::string, double> map) {

	typedef std::pair<std::string, double> pair;

	// empty vector of pairs
	std::vector<pair> vec;

	// copy key-value pairs from the map to the vector
	std::copy(map.begin(),
		map.end(),
		std::back_inserter<std::vector<pair>>(vec));

	// sort the vector by increasing order of its pair's second value
	// if second value are equal, order by the pair's first value
	std::sort(vec.begin(), vec.end(),
		[](const pair& l, const pair& r) {
		if (l.second != r.second)
			return l.second > r.second;
		return l.first > r.first;
	});
	return vec;

}

int sumOfMapVal(std::unordered_map<std::string, double> um) {
	const size_t sum = std::accumulate(std::begin(um)
		, std::end(um)
		, 0
		, [](int value, const std::unordered_map<std::string, double>::value_type& p)
	{ return value + p.second; });
	return sum;
}

std::vector<std::pair<std::string, double>> calcPersentage(std::vector<std::pair<std::string, double>> arr, int sum) {

	// iterator
	std::vector<std::pair<std::string, double> >::iterator itr = arr.begin();
	for (; itr != arr.end(); itr++) {
		(*itr).second = (*itr).second / sum * 100;
		//round
		(*itr).second = (int)((*itr).second * 100 + .5);
		(*itr).second = (double)(*itr).second / 100;
	}
	return arr;
}
void pintGraph(std::vector<std::pair<std::string, double>>input, int barCount ) {

	std::vector<double> vector1;

	std::cout << "print barchart" << std::endl << std::endl;

	for (int j = 0; j <= barCount-1; j++) {
		if (input.size() <= j) {
			break;
		}
		std::vector<std::pair<std::string, double>>::iterator itr = input.begin() + j;
		vector1.push_back((*itr).second);
		
	}

	std::string xlegend = "          ";
	
	for (int i = 0; i < vector1.size(); i++) {
		xlegend.append(input[i].first);
		xlegend.append("  ");
	}
	//fill vector for x axis
	auto max0 = vector1.size() * 6;
	for (int i = 0; i < max0; i += 2) {
		vector1.insert(vector1.begin() + i, vector1.at(i));
		i += 2;
		vector1.insert(vector1.begin() + i, 2, 0);
		i += 2;
		vector1.insert(vector1.begin() + i, 2, 0);
	}


	vector1.insert(vector1.begin(), 2, 0);

	double max1 = *max_element(vector1.begin(), vector1.end());
	double max2 = *max_element(vector1.begin(), vector1.end());
	for (double i = 0; i < max1; i += max1 / 10) {
		
		std::cout << std::right << std::setfill(' ') << std::setw(8)  << std::setprecision(2) <<(max1 ) - i << "%";
		for (int j = 0; j < vector1.size(); j ++ ) {
			if (vector1.at(j) >= max2 ) {
				std::cout << '*';
			}
			else {
				std::cout << ' ';
			}
		}
		max2 -= max1 / 10;
		std::cout << std::endl;
	}
	

	std::cout << xlegend << std::endl;
}

