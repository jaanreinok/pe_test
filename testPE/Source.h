#pragma once


std::vector<std::string> readInputFile(std::string fileName);
std::string& removeChars(std::string& s, const std::string& chars);
std::unordered_map<std::string, double> proccessWord(std::string word, int minSnippet);
std::unordered_map<std::string, double> mergeMap(std::unordered_map<std::string, double> um1, std::unordered_map<std::string, double> um2);
std::vector<std::pair<std::string, double>> sortMaptoVec(std::unordered_map<std::string, double> map);
int sumOfMapVal(std::unordered_map<std::string, double> um);
std::vector<std::pair<std::string, double>> calcPersentage(std::vector<std::pair<std::string, double>> arr, int sum);
void pintGraph(std::vector<std::pair<std::string, double>>input, int barCount);
