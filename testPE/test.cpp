#include "stdafx.h"
#include "catch.hpp"
#include "Source.h"



TEST_CASE("Test if input file is not empty or unable to open)", "[readInputFile]") {


	REQUIRE_NOTHROW(readInputFile("input.txt"), "Unable to open file");
	REQUIRE_NOTHROW(readInputFile("input.txt"), "File is empty");


	
}

TEST_CASE("test if proccessWord  fixed case works )", "[proccessWord]") {


	std::unordered_map<std::string, double> umTest{ { "all",1 },{ "lla",1 },{ "alla",1 } };
	REQUIRE(proccessWord("alla?", 3) == umTest);

}

TEST_CASE("test of method mergeMap)", "[mergeMap]") {

	std::unordered_map<std::string, double> umTestExpected{ { "all",2 },{ "lla",2 },{ "alla",2 } };
	std::unordered_map<std::string, double> um1{ { "all",1 },{ "lla",1 },{ "alla",1 } };
	std::unordered_map<std::string, double> um2{ { "all",1 },{ "lla",1 },{ "alla",1 } };
	std::unordered_map<std::string, double> um3;


	WARN("print first map");

	std::cout << std::endl;
	for (const auto &p : um1)
	{
		std::cout << "{ " << p.first << ", " << p.second << " } ";
	}

	WARN("print second map");

	for (const auto &p : um2)
	{
		std::cout << "{ " << p.first << ", " << p.second << " } ";
	}
	um3 = mergeMap(um1, um2);

	WARN("print third map");

	std::cout << std::endl;
	for (const auto &p : um3)
	{
		std::cout << "{ " << p.first << ", " << p.second << " } ";
	}
	std::cout << std::endl;

	REQUIRE(um3 == umTestExpected);
}
TEST_CASE("test method sortMaptoVec)", "[sortMaptoVec]") {
	std::unordered_map<std::string, double> um{ { "all",1 },{ "lla",2 },{ "alla",3 } };
	std::vector<std::pair<std::string, double>> expRes{ { "alla",3 } ,{ "lla",2 },{ "all",1 } };

	REQUIRE(sortMaptoVec(um) == expRes);
}

TEST_CASE("test of sumOfMapVal)", "[sumOfMapVal and sortMaptoVec]") {


	std::unordered_map<std::string, double> um{ { "all",1 },{ "lla",1 },{ "alla",2 } };


	std::vector<std::pair<std::string, double>> vec = sortMaptoVec(um);

	int sum = sumOfMapVal(um);
	REQUIRE(sum == 4);
	
}

TEST_CASE("integration)", "[integration]") {

	std::vector<std::pair<std::string, double>> expOutput{
	{ "ella", 28.57 },
	{ "kella", 14.29 },
	{ "kell", 14.29 },
	{ "bella", 14.29 },
	{ "bell", 14.29 },
	{ "anna", 14.29 } };


	std::vector<std::string> inputVec = readInputFile("input.txt");
	std::unordered_map<std::string, double> umText;
	for (auto p : inputVec) {
		std::unordered_map<std::string, double> umWord = proccessWord(p, 4);
		umText = mergeMap(umText, umWord);
	}
	std::vector<std::pair<std::string, double>> vec = sortMaptoVec(umText);

	int sum = sumOfMapVal(umText);

	std::vector<std::pair<std::string, double>> sorted = sortMaptoVec(umText);
	std::vector<std::pair<std::string, double>> sortedCalc = calcPersentage(sorted, sum);
	for (auto p : sortedCalc) {
		std::cout << std::endl;
		std::cout << p.first << " : " << p.second << "%" << std::endl;
	}
	pintGraph(expOutput, 10);
	REQUIRE(sortedCalc == expOutput);
}
TEST_CASE("benchmarked", "[benchmark]") {


	BENCHMARK("merge vectors") {
		std::vector<std::string> inputVec = readInputFile("input2.txt");
		std::unordered_map<std::string, double> umText;
		for (auto p : inputVec) {
			std::unordered_map<std::string, double> umWord = proccessWord(p, 4);
			umText = mergeMap(umText, umWord);
		}
		REQUIRE(umText.size() == 6);
	}

}
