// testPE.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*
Teha programm, mis võtab sisendiks suvalise sisuga tekstifaili ja loeb selles kokku kõik seal esinevad rohkem kui 3 tähelised täheühendid sõnades.
Väljundiks väljastab programm kuni 10 enimesinevat täheühendit koos nende esinevuse protsendiga tabelina ja tulpdiagrammil.
*/


#ifdef UNIT_TESTING


#define CATCH_CONFIG_RUNNER
#include "catch.hpp"


// this is the main() used for unit testing
int main(int argc, char* argv[])
{
	Catch::Session session; // There must be exactly one instance

	int returnCode = session.applyCommandLine(argc, argv);
	if (returnCode != 0) // Indicates a command line error
		return returnCode;


	return session.run();
}

#else
#include "stdafx.h"
#include "Source.h"

// actual application entry point
int main(void)
{
	std::string inputFile;
	std::cout << "Enter input file path" << std::endl;
	std::cin >> inputFile;
	//Read file to sequense container. Method parses input to separate words by whitespace. 
	std::vector<std::string> inputVec;
	try
	{
		inputVec = readInputFile(inputFile);
		
	}
	catch (std::invalid_argument& e)
	{
		std::cout << e.what() << std::endl;
		std::getchar();
		std::getchar();
		return -1;
	}

	//As appears from tests is not efficient way. Loops trough sequence container and merges input to unordered associative container.
	std::unordered_map<std::string, double> umText;
	for (auto p : inputVec) {
		//proccessWord filteres symbols.
		std::unordered_map<std::string, double> umWord = proccessWord(p, 4);
		umText = mergeMap(umText, umWord);
	}
	
	//Sums umap by second value.
	int sum = sumOfMapVal(umText);
	//Sorts map to vector
	std::vector<std::pair<std::string, double>> sortedVec = sortMaptoVec(umText);
	//calculate precentage;
	std::vector<std::pair<std::string, double>> sortedCalc = calcPersentage(sortedVec, sum);
	std::cout << "print table" << std::endl;

	
	for (int j = 0; j <= 10 - 1; j++) {
		if (sortedCalc.size() <= j) {
			break;
		}
		std::vector<std::pair<std::string, double>>::iterator itr = sortedCalc.begin() + j;
		std::cout << (*itr).first << " : " << (*itr).second << "%" << std::endl;
	}
	//over 10 formatting will fail. depends on cmd window
	pintGraph(sortedCalc, 10); 
	//stop from colsing
	std::getchar();
	std::getchar();
	return 0;
}

#endif